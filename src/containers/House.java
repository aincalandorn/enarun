/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package containers;

import java.util.ArrayList;

/**
 *
 * @author 631734
 */
public class House {

	private int revealed = 0;
	private Card first;
	private Card second;
	private Card third;

	public String getHand() {
				
		String ret = "<strong>The house's hand: </strong>";
		for(int x=0;x<1;x++){
			if (revealed > 0) {
			switch(revealed){
				case 1:
					ret += first.getCard() + ", [hidden], [hidden]";
					break;
				case 2:
					ret += first.getCard() + ", " + second.getCard() + ", [hidden]";
					break;
				case 3:
					ret += first.getCard() + ", " + second.getCard() + ", "
							+ third.getCard();
					break;
				default:
					System.out.println("Unknown revealed count?");
				}
			}//end revealed 
			else {
				ret += "[hidden], [hidden], [hidden]";
			}
		}
		return ret;
	}

	public void setCards(Card c1, Card c2, Card c3) {
		revealed=0;
		first = c1;
		second = c2;
		third = c3;
	}

	public void reveal(int reveal) {
		this.revealed=reveal;
	}

	public String allCards() {
		String ret = "The houses cards are: ";

		ret += first.getCard() + ", " + second.getCard() + ", "
				+ third.getCard();
		ret += "\n<br />The revealed card count is: "+revealed+"<br />";
		return ret;
	}

	public House() {

	}
	public void revealAll() {
		for(int x = 0;x<4;x++){
			reveal(x);
			getHand();
		}
	}

	public ArrayList<Card> getCards(){
		ArrayList<Card> cards = new ArrayList<>();
		cards.add(first);
		cards.add(second);
		cards.add(third);
		return cards;
	}
}
